package com.chlwodh97.insuranceconsult_api.model;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ConslutItem {
    private LocalDate conDate;
    private String name;
    private String birth;
    private Boolean isMan;

}
