package com.chlwodh97.insuranceconsult_api.model;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ConsultResponse {
    private Long id;
    private LocalDate conDate;
    private String name;
    private String birth;
    private String isManName;
    private String conTime;
    private String phoneNumber;
}
