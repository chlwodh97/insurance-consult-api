package com.chlwodh97.insuranceconsult_api.controller;


import com.chlwodh97.insuranceconsult_api.model.ConslutItem;
import com.chlwodh97.insuranceconsult_api.model.ConsultRequest;
import com.chlwodh97.insuranceconsult_api.model.ConsultResponse;
import com.chlwodh97.insuranceconsult_api.service.ConsultService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/consult")
public class ConsultController {

    private final ConsultService consultService;

    @PostMapping("/new")
    public String setConsult (@RequestBody ConsultRequest request){
        consultService.setConsult(request);

        return "회원 정보 꿀꺽";
    }


    @GetMapping("/all")
    public List<ConslutItem> getConsults() {
        return consultService.getConsult();
    }

    @GetMapping("/detail/{id}")
    public ConsultResponse getConsult(@PathVariable long id){
        return consultService.getConsult(id);
    }

}
