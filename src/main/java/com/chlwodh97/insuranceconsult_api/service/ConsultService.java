package com.chlwodh97.insuranceconsult_api.service;


import com.chlwodh97.insuranceconsult_api.entity.Consult;
import com.chlwodh97.insuranceconsult_api.model.ConslutItem;
import com.chlwodh97.insuranceconsult_api.model.ConsultRequest;
import com.chlwodh97.insuranceconsult_api.model.ConsultResponse;
import com.chlwodh97.insuranceconsult_api.repository.ConsultRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ConsultService {

    private final ConsultRepository consultRepository;


    public void setConsult(ConsultRequest request){

        Consult addData = new Consult();
        addData.setConDate(request.getConDate());
        addData.setName(request.getName());
        addData.setBirth(request.getBirth());
        addData.setIsMan(request.getIsMan());
        addData.setConTime(request.getConTime());
        addData.setPhoneNumber(request.getPhoneNumber());

        consultRepository.save(addData);
    }

    public List<ConslutItem> getConsult() {
        List<Consult> originList = consultRepository.findAll();

        List<ConslutItem> result = new LinkedList<>();
        for (Consult consult : originList) {
            ConslutItem addItem = new ConslutItem();
            addItem.setConDate(consult.getConDate());
            addItem.setName(consult.getName());
            addItem.setBirth(consult.getBirth());
            addItem.setIsMan(consult.getIsMan());

            result.add(addItem);
        }
        return result;
    }

    public ConsultResponse getConsult(long id){
        Consult originData = consultRepository.findById(id).orElseThrow();

        ConsultResponse response = new ConsultResponse();
        response.setId(originData.getId());
        response.setConDate(originData.getConDate());
        response.setName(originData.getName());
        response.setBirth(originData.getBirth());
        response.setIsManName(originData.getIsMan()? "예" : "아니오");
        response.setConTime(originData.getConTime());
        response.setPhoneNumber(originData.getPhoneNumber());

        return  response;
    }
}

