package com.chlwodh97.insuranceconsult_api.repository;

import com.chlwodh97.insuranceconsult_api.entity.Consult;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsultRepository extends JpaRepository<Consult , Long> {
}
