package com.chlwodh97.insuranceconsult_api.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Consult {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate conDate;

    @Column(nullable = false , length = 20)
    private String name;

    @Column(nullable = false)
    private String birth;

    @Column(nullable = false)
    private Boolean isMan;

    @Column(nullable = false)
    private String conTime;

    @Column(nullable = false)
    private String phoneNumber;
}
